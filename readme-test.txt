Subject : 01-get
Created : 20200507
Author : dickson.cheng

path : C:\myprogram\java\ivy\2.4\tutorial\01-get>

Ref:
Get task
https://ant.apache.org/manual/Tasks/get.html

Output:
-------
C:\myprogram\java\ivy\2.4\tutorial\01-get>ant
Buildfile: C:\myprogram\java\ivy\2.4\tutorial\01-get\build.xml

run:
    [mkdir] Created dir: C:\myprogram\java\ivy\2.4\tutorial\01-get\help
      [get] Getting: https://ant.apache.org/
      [get] To: C:\myprogram\java\ivy\2.4\tutorial\01-get\help\index.html

BUILD SUCCESSFUL
Total time: 4 seconds

C:\myprogram\java\ivy\2.4\tutorial\01-get>